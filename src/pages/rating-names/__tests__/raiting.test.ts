import { expect, test } from '@playwright/test';

test('Проверяем, что отображается текст ошибки ‘Ошибка загрузки рейтинга’ после блокирования запроса: ', async ({ page, browser }) => {
  // Блокируем запрос к методу получения рейтинга для отображения ошибки
  await page.route('**/api/likes/cats/rating', route => route.abort());

  // Переходим на страницу рейтинга
  await page.goto('https://meowle.fintech-qa.ru/rating');

  // Ждем появления текста ошибки загрузки рейтинга
  const errorElement = await page.waitForSelector('.ajs-error');

  // Полаем текст ошибки
  const errorText = await page.evaluate(element => element.textContent, errorElement);
  console.log("Текст ошибки:", errorText);

  // Проверяем, что отображается текст ошибки загрузки рейтинга
  expect(errorText).toContain('Ошибка загрузки рейтинга');
});

test('Рейтинг котиков отображается по убыванию?', async ({ page }) => {
  // Переходим на страницу рейтинга
  await page.goto('https://meowle.fintech-qa.ru/rating');

  // Задержка перед началом выполнения теста (3 секунды)
  await new Promise(resolve => setTimeout(resolve, 3000));

  // Создаем пустой массив для хранения текстовых значений рейтинга
  const ratingsText = [];

  // Проходимся по каждому элементу и извлекаем текстовое содержимое
  for (let i = 1; i <= 10; i++) {

    // Формируем XPath для текущего элемента
    const xpath = `/html/body/div[1]/div/section[2]/div/div/div[2]/div[2]/table[1]/tbody/tr[${i}]/td[3]/text()`;

    // Получаем текстовое содержимое текущего элемента
    const ratingTextElement = await page.evaluate((xpath) => {
      const textNode = document.evaluate(
        xpath,
        document,
        null,
        XPathResult.STRING_TYPE,
        null
      ).stringValue;
      return textNode.trim();
    }, xpath);

    // Пропускаем пустые строки
    if (ratingTextElement !== '') {
      // Добавляем значение в массив
      ratingsText.push(ratingTextElement);
    }
  }

  // Выводим массив лайков в нужном формате в консоль
  console.log("Массив лайков котиков:");
  ratingsText.forEach((value, index) => {
    console.log(`${index + 1}. ${value}`);
  });

  // Проверяем, что рейтинг отображается по убыванию
  let isDescending = true;
  for (let i = 0; i < ratingsText.length - 1; i++) {
    if (parseInt(ratingsText[i]) < parseInt(ratingsText[i + 1])) {
      isDescending = false;
      break;
    }
  }

  if (isDescending) {
    console.log("Рейтинг отображается по убыванию.");
  } else {
    console.log("Рейтинг НЕ отображается по убыванию.");
  }    
});